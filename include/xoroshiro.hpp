/**
 * xoroshiro.hpp: Xor-Rotate-Shift-Rotate pseudo random number generator.
 *
 * Copyright (c) 2016-2018 Masashi Fujita
 */
#pragma once
#ifndef xoroshiro_hpp__d7ef202791dd40f6bf28b734378c9574
#define xoroshiro_hpp__d7ef202791dd40f6bf28b734378c9574  1

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <atomic>

namespace XoRoShiRo {

    using uint128_t = unsigned __int128 ;

    class state_t ;

    namespace detail {
        constexpr uint64_t rotl (uint64_t v, int count) {
            auto c = static_cast<uint8_t> (count) ;
            return (v << c) | (v >> (64u - c)) ;
        }

        constexpr uint128_t to_uint128 (uint64_t v0, uint64_t v1) {
            return ( (static_cast<uint128_t> (v0) << 0u)
                   | (static_cast<uint128_t> (v1) << 64u)) ;
        }
    }

    class alignas (16) lockfree_state_t final {
        uint64_t s0_ = 0 ;
        uint64_t s1_ = 1 ;
    public:
        lockfree_state_t () noexcept = default ;

        lockfree_state_t (uint64_t s0, uint64_t s1) noexcept : s0_ { s0 }, s1_ { s1 } {
            /* NO-OP */
        }

        lockfree_state_t (const lockfree_state_t &s) noexcept = default ;

        explicit lockfree_state_t (const state_t &s) noexcept ;

        lockfree_state_t &  operator = (const lockfree_state_t &s) noexcept ;

        lockfree_state_t &  setState (uint64_t s0, uint64_t s1) noexcept ;

        uint64_t next () noexcept {
            while (true) {
                using namespace detail ;
                auto saved_state = asUInt128 () ;
                const uint64_t s0     = s0_ ;
                uint64_t       s1     = s1_ ;
                const uint64_t result = s0 + s1;

                s1 ^= s0;
                auto next_state = to_uint128 ( rotl (s0, 55) ^ s1 ^ (s1 << 14u)
                                             , rotl (s1, 36)) ;
                if (asTarget ()->compare_exchange_strong (saved_state, next_state)) {
                    return result ;
                }
            }
        }

        lockfree_state_t &  jump () noexcept ;

        uint128_t asUInt128 () const noexcept {
            return *((uint128_t *)(this)) ;
        }
    private:

        volatile std::atomic<uint128_t> *   asTarget () noexcept {
            return (volatile std::atomic<uint128_t> *)(this) ;
        }

        uint64_t unsafe_next () noexcept {
            using namespace detail ;
            const uint64_t s0     = s0_ ;
            uint64_t       s1     = s1_ ;
            const uint64_t result = s0 + s1;

            s1 ^= s0;
            s0_ = rotl (s0, 55) ^ s1 ^ (s1 << 14u);
            s1_ = rotl (s1, 36);

            return result;
        }

        void check_padding () noexcept {
            static_assert (offsetof (lockfree_state_t, s1_) == 8, "No padding allowed between slots") ;
        }
    };

    class state_t final {
        friend class lockfree_state_t ;

        uint64_t s0_ = 0 ;
        uint64_t s1_ = 1 ;
    public:
        state_t () noexcept = default ;

        state_t (uint64_t s0, uint64_t s1) noexcept : s0_ {s0}, s1_ {s1} {
            /* NO-OP */
        }

        state_t (const state_t &s) noexcept = default ;

        state_t &   operator = (const state_t &s) noexcept = default ;

        state_t &   setState (uint64_t s0, uint64_t s1) noexcept {
            s0_ = s0 ;
            s1_ = s1 ;
            return *this ;
        }

        uint64_t next () noexcept {
            using namespace detail ;
            const uint64_t s0     = s0_ ;
            uint64_t       s1     = s1_ ;
            const uint64_t result = s0 + s1;

            s1 ^= s0;
            s0_ = rotl (s0, 55) ^ s1 ^ (s1 << 14u);
            s1_ = rotl (s1, 36);

            return result;
        }

        state_t &   jump () noexcept {
            uint_fast64_t s0 = 0;
            uint_fast64_t s1 = 0;

            auto update = [&s0, &s1] (state_t &S, uint64_t mask) {
                for (int_fast32_t b = 0 ; b < 64 ; ++b) {
                    auto v0 = S.s0_ ;
                    auto v1 = S.s1_ ;
                    if ((mask & (1ull << b)) != 0) {
                        s0 ^= v0;
                        s1 ^= v1;
                    }
                    S.next () ;
                }
            };
            update (*this, 0xBEAC0467EBA5FACBull);
            update (*this, 0xD86B048B86AA9922ull);
            s0_ = s0;
            s1_ = s1;
            return *this ;
        }
    } ;


    inline lockfree_state_t &lockfree_state_t::jump () noexcept {
        using namespace detail ;
        uint_fast64_t s0 ;
        uint_fast64_t s1 ;

        auto update = [&s0, &s1](state_t &S, uint64_t mask) {
            for (uint_fast32_t b = 0 ; b < 64 ; ++b) {
                auto v0 = S.s0_ ;
                auto v1 = S.s1_ ;
                if ((mask & (1ull << b)) != 0) {
                    s0 ^= v0 ;
                    s1 ^= v1 ;
                }
                S.next () ;
            }
        } ;
        while (true) {
            auto    saved_state = asUInt128 ();
            state_t tmp_state { s0_, s1_ };
            s0 = 0 ;
            s1 = 0 ;
            update (tmp_state, 0xBEAC0467EBA5FACBull);
            update (tmp_state, 0xD86B048B86AA9922ull);
            auto next_state = to_uint128 (s0, s1) ;
            if (asTarget ()->compare_exchange_strong (saved_state, next_state)) {
                return *this;
            }
        }
    }
}

#endif /* xoroshiro_hpp__d7ef202791dd40f6bf28b734378c9574 */
