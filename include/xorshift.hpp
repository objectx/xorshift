/**
 * xorshift.hpp: XorShift pseudo random number generator.
 *
 * Copyright (c) 2016-2018 Masashi Fujita
 */
#pragma once
#ifndef xorshift_hpp__747bdbd20b5845a1ab6e4abd48744faa
#define xorshift_hpp__747bdbd20b5845a1ab6e4abd48744faa  1

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <atomic>

namespace XorShift {

    using uint128_t = unsigned __int128 ;

    namespace detail {
        constexpr uint128_t to_uint128 (uint64_t v0, uint64_t v1) noexcept {
            return ( (static_cast<uint128_t> (v0) <<  0u)
                   | (static_cast<uint128_t> (v1) << 64u)) ;
        }
    }

    class state_t ;

    class alignas (16) lockfree_state_t final {
        uint64_t s0_ = 0 ;
        uint64_t s1_ = 1 ;
    public:
        lockfree_state_t () noexcept = default ;

        lockfree_state_t (uint64_t s0, uint64_t s1) noexcept : s0_ { s0 }, s1_ { s1 } {
            /* NO-OP */
        }

        lockfree_state_t (const lockfree_state_t &s) noexcept = default ;

        explicit lockfree_state_t (const state_t &s) noexcept ;

        lockfree_state_t &  operator = (const lockfree_state_t &s) noexcept ;

        lockfree_state_t &  setState (uint64_t s0, uint64_t s1) noexcept ;

        uint64_t next () noexcept {
            while (true) {
                using namespace detail;
                auto                saved_state = asUInt128 ();
                uint_fast64_t       s1          = s0_;
                const uint_fast64_t s0          = s1_;
                s1 ^= s1 << 23u;
                uint_fast64_t v1         = s1 ^s0 ^(s1 >> 18u) ^(s0 >> 5u);
                auto          next_state = to_uint128 (s0, v1);
                if (asTarget ()->compare_exchange_strong (saved_state, next_state)) {
                    return v1 + s0;
                }
            }
        }

        lockfree_state_t &  jump () noexcept ;

        uint128_t asUInt128 () const noexcept {
            return *((uint128_t *)(this)) ;
        }
    private:

        volatile std::atomic<uint128_t> *   asTarget () noexcept {
            return (volatile std::atomic<uint128_t> *)(this) ;
        }

        void check_padding () noexcept {
            static_assert (offsetof (lockfree_state_t, s1_) == 8, "No padding allowed between slots") ;
        }
    };


    class state_t final {
        friend class lockfree_state_t ;

        uint64_t s0_ = 0 ;
        uint64_t s1_ = 1 ;
    public:
        state_t () noexcept = default ;

        state_t (uint64_t s0, uint64_t s1) noexcept : s0_ {s0}, s1_ {s1} {
            /* NO-OP */
        }

        state_t (const state_t &s) noexcept = default ;

        state_t &   operator = (const state_t &s) noexcept = default ;

        state_t &   setState (uint64_t s0, uint64_t s1) noexcept {
            s0_ = s0 ;
            s1_ = s1 ;
            return *this ;
        }

        uint64_t next () noexcept {
            uint_fast64_t s1 = s0_ ;
            const uint_fast64_t s0 = s1_ ;
            uint64_t v0 = s0 ;
            s1 ^= s1 << 23u ;
            uint64_t v1 = s1 ^ s0 ^ (s1 >> 18u) ^ (s0 >> 5u) ;
            s0_ = v0 ;
            s1_ = v1 ;
            return v1 + s0 ;
        }

        state_t &   jump () noexcept {
            uint_fast64_t s0 = 0 ;
            uint_fast64_t s1 = 0 ;

            auto update = [&s0, &s1, this](state_t &S, uint64_t mask) {
                for (uint_fast32_t b = 0 ; b < 64 ; ++b) {
                    auto v0 = S.s0_ ;
                    auto v1 = S.s1_ ;
                    if ((mask & (1ull << b)) != 0) {
                        s0 ^= v0 ;
                        s1 ^= v1 ;
                    }
                    S.next () ;
                }
            } ;
            update (*this, 0x8a5cd789635d2dffull) ;
            update (*this, 0x121fd2155c472f96ull) ;
            s0_ = s0 ;
            s1_ = s1 ;
            return *this ;
        }
    } ;

    inline lockfree_state_t::lockfree_state_t (const state_t &s) noexcept : s0_ { s.s0_ }, s1_ { s.s1_ } {
        /* NO-OP */
    }

    inline lockfree_state_t &lockfree_state_t::jump () noexcept {
        using namespace detail ;
        uint_fast64_t s0 ;
        uint_fast64_t s1 ;

        auto update = [&s0, &s1](state_t &S, uint64_t mask) {
            for (uint_fast32_t b = 0 ; b < 64 ; ++b) {
                auto v0 = S.s0_ ;
                auto v1 = S.s1_ ;
                if ((mask & (1ull << b)) != 0) {
                    s0 ^= v0 ;
                    s1 ^= v1 ;
                }
                S.next () ;
            }
        } ;
        while (true) {
            auto    saved_state = asUInt128 ();
            state_t tmp_state { s0_, s1_ };
            s0 = 0 ;
            s1 = 0 ;
            update (tmp_state, 0x8A5CD789635D2DFFull);
            update (tmp_state, 0x121FD2155C472F96ull);
            auto next_state = to_uint128 (s0, s1) ;
            if (asTarget ()->compare_exchange_strong (saved_state, next_state)) {
                return *this;
            }
        }
    }
}

#endif /* end of include guard: xorshift_hpp__747bdbd20b5845a1ab6e4abd48744faa */
