
cmake_minimum_required (VERSION 3.3)

find_package (Threads REQUIRED)

set (TEST_SOURCES xorshift128.cpp xoroshiro128.cpp prng.cpp main.cpp)

add_executable (test_xorshift ${TEST_SOURCES})
    target_link_libraries (test_xorshift PRIVATE xorshift Threads::Threads)
    target_compile_options (test_xorshift PRIVATE -mcx16)

set_property (TARGET test_xorshift
              APPEND
              PROPERTY LINK_FLAGS -mcx16)

if (COMMAND cotire)
    cotire (test_xorshift)
endif ()

add_test (NAME test_xorshift
          COMMAND test_xorshift -r compact)
